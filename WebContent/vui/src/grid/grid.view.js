/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */
;(function(){

/**
 * @ignore
 * @private
 */
VUI.Class('GridView',{
	init:function(grid) {
		this._super(grid);
		this.grid = grid;
		var id = grid.id;
		this.idField = this.opt('idField') || ('idField_' + id);
		this.checkTdWidth = 30;
		this.checkboxTrigger = 'checkboxChange';
		this.radioTrigger = 'radioChange';
		
		this.selectCache = grid.getSelectCache();
		
		this.v_vuitableth = this.buildDirectiveName('vuitableth');
		this.v_vuitabletr = this.buildDirectiveName('vuitabletr');
		this.v_vuitabletd = this.buildDirectiveName('vuitabletd');
		
	}
	,reset:function() {
		if(this.$checkAll) {
			this.$checkAll.prop('checked',false);
		}
	}
	,initDirective:function() {
		this.initHeadDirective();
		this.initTrDirective();
		this.initCellDirective();
	}
	,initHeadDirective:function() {
		var that = this;
		var grid = this.grid;
		
		this.directive(this.v_vuitableth,function(value,$th){
			var header = value.header;
			var halign = header.halign || header.align;
			halign = halign || 'left';
			if(halign) {
				$th.css('text-align',halign);
			}
			if(header.hidden === true) {
				$th.hide();
			}
			
			if(header.width) {
				$th.width(parseInt(header.width));
			}
			
			if(header.checkbox) {
				if(that.isMultiSelect()) {
					var $checkAll = $('<input type="'+that.getCheckType()+'">');
					$checkAll.bind('doClick',function(e,checked){
						var $checkboxs = checked ? that.getUnCheckCheckbox() : that.getCheckedCheckbox();
						var fireEventName = checked ? 'CheckAll' : 'UncheckAll';
						$checkboxs.each(function(){
							this.checked = checked;
							$(this).trigger(that.checkboxTrigger);
						});
						
						that.grid.fire(fireEventName,{rows:that.grid.getRows(),$target:$checkAll});
					})
					
					$checkAll.click(function(){
						$(this).trigger('doClick',[this.checked]);
					});
					
					that.$checkAll = $checkAll;
					
					$th.html($checkAll).css('text-align','center');
				}
				
				$th.width(that.checkTdWidth);
			}
			// 排序
			if(header.sortable) {
				var currentSortOrder = that.sortOrder;
				var sortName = header.field;
				$th.css({cursor:'pointer'}).click(function(){
					var $span = $(this).find('span');
					if(currentSortOrder == 'ASC') {
						$span.html('▼');
						currentSortOrder = "DESC";
					}else{
						$span.html('▲');
						currentSortOrder = "ASC";
					}		
					that.grid.sort(sortName,currentSortOrder);
				});
			}
		});
	}
	,initTrDirective:function() {
		var that = this;
		var $before = null;
		this.directive(this.v_vuitabletr,function(value,$tr){
			var header = value.header;
			var row = value.row;
			if(that.opt('clickHighlight')) {
				$tr.on('click',function(e) {
					e.stopPropagation();
					// 点击行高亮
					if($before) {
						$before.removeClass('ui-state-highlight');
					}
					
					if(!$tr.hasClass('ui-state-active')) {
						$tr.addClass('ui-state-highlight');
					}
					$before = $tr;
				});
			}
			
			var $checkbox = $tr.find('.vui-gird-check').eq(0);
			if($checkbox.length > 0) {
				if(that.isSelectCache()) {
					var value = $checkbox.val();
					var isInCache = that.grid.isInCache(value);
					$checkbox.prop('checked',isInCache);
					$checkbox.trigger('active');
				}
				// 如果为true，当用户点击行的时候该复选框就会被选中或取消选中。
				// 如果为false，当用户仅在点击该复选框的时候才会呗选中或取消。
				if(that.opt('checkOnSelect')) {
					$tr.on('click',function(e) {
						e.stopPropagation();
						var checked = !$checkbox.prop('checked');
						$checkbox.prop('checked',checked);
						$checkbox.trigger(that.getTriggerName());
					});
					
				}
			}
			
		});
	}
	,isOddTr:function(index) {
		var isOdd = (index + 1)%2 == 0;
		// 显示斑马线
		return this.opt('striped') && isOdd;
	}
	,initCellDirective:function() {
		var that = this;
		// 自定义指令,作用在class上
		this.directive(this.v_vuitabletd,function(value,$td){
			
			var header = value.header;
			var row = value.row; 
    		var val = row[header.field];
    		var index = value.index;

			var align = header.align || 'left';
			if(align) {
				$td.attr('align',align);
			}
			if(header.hidden === true) {
				$td.hide();
			}
			if(header.styler) {
	    		$td.attr('style',header.styler(val,row,index));
	    	}
			// 文本在同一行
			if(that.opt('nowrap')) {
				$td.css({'whiteSpace':'nowrap'});
			}else{
				$td.css({'whiteSpace':'normal','wordWrap':'break-word'});
			}
			
			if(header.checkbox) {
				that.buildSelections($td,index,row,header);
				return;
			}
			
	    	if(header.formatter) {
	    		val = header.formatter(val,row,index,$td);
	    	}
	    	
	    	$td.html(val);
		});
	}
	// 构建选择框
	,buildSelections:function($td,index,row,header) {
		var that = this;
		var clickHandler = that.isSingleSelect()
			? this.radioClickHandler 
			: this.checkboxClickHandler;
		
		var pkId = header.field || this.idField;
		this.opt('idField',pkId);
		var value = row[pkId] || ('value_' + index + "_" + that.grid.page.pageIndex);
		
		$td.addClass('pui-datatable-tdcheck')
		var initChecked = row.selected ? ' checked="checked" ' : '';
		// 创建
		var $checkbox = $('<input type="'+that.getCheckType()+'" ' +
				'class="vui-gird-check" ' +
				'data-index="'+index+'" ' +
				'name="' + that.idField + '" ' +
				'value="'+value+'" ' +
				''+initChecked+'>')
			.click(function(e){
				e.stopPropagation();
				
				clickHandler.call(that,$(this),index,row);
		});
		
		var param = {index:index,row:row,$target:$checkbox};
		
		// 添加自定义事件
		$checkbox.bind(that.checkboxTrigger,function(){
			var checked = this.checked;
			var eventName = checked ? 'Check' : 'Uncheck';
			var $cbx = $(this);
			
			that.setCheckboxCache($cbx,row);
			
			$cbx.trigger('active');
			
			that.grid.fire(eventName,param);
		});
		
		$checkbox.bind('active',function(){
			if(that.isSingleSelect()) {
				that.setRadioActive($(this));
			}else{
				that.setCheckboxActive($(this));
			}
		});
		
		$checkbox.bind(that.radioTrigger,function(){
			var checked = this.checked;
			var eventName = checked ? 'Select' : 'UnSelect';
			var $radio = $(this);
			var $tr = $radio.parents('tr');
			
			that.setRadioCache($radio,row);
			that.setRadioActive($radio);
			
			that.grid.fire(eventName,param);
			
		});
		
		$td.html($checkbox).css('text-align','center');
	}
	,setCheckboxActive:function($selector) {
		var checked = $selector.prop('checked');
		var $tr = $selector.parents('tr').eq(0);
		if(checked) {
			$tr.removeClass('ui-state-highlight').addClass('ui-state-active');
		}else{
			$tr.removeClass('ui-state-active');
		}
		
		if(this.isMultiSelect()) {
			this.doCheckedAllEffect($selector);
		}
	}
	,setRadioActive:function($radio) {
		var checked = $radio.prop('checked');
		var $tr = $radio.parents('tr');
		
		if(checked) {
			if(this.$_before) {
				this.$_before.removeClass('ui-state-active');
			}
			this.$_before = $tr.removeClass('ui-state-highlight').addClass('ui-state-active');
		}else{
			this.$_before = null;
			$tr.removeClass('ui-state-active');
		}
	}
	,setCheckboxCache:function($checkbox,row) {
		if(this.isSelectCache()) {
			this.getSelectCache()[$checkbox.val()] = $checkbox.prop('checked') ? row : false;
		}
	}
	,setRadioCache:function($radio,row) {
		if(this.isSelectCache()) {
			this.grid.resetSelectCache();
			this.getSelectCache()[$radio.val()] = $radio.prop('checked') ? row : false;
		}
	}
	,getSelectCache:function() {
		return this.grid.getSelectCache();
	}
	,isSelectCache:function() {
		return this.grid.isSelectCache();
	}
	// 单选事件
	,radioClickHandler:function($radio,index,row) {
		$radio.trigger(this.radioTrigger);
	}
	// 多选事件
	,checkboxClickHandler:function($checkbox,index,row) {
		$checkbox.trigger(this.checkboxTrigger);
	}
	// 当所有行勾选时,全选也应该勾选
	,doCheckedAllEffect:function($checkbox) {
		if(!$checkbox.prop('checked')) {
			this.$checkAll.prop('checked',false);
			return;
		}
//		var isCheckedAll = this.isCheckedAll();
//		this.$checkAll.prop('checked',isCheckedAll);
	}
	,isCheckedAll:function() {
		var currentCheckLen = this.getCheckedCheckbox().length;
		var rowLen = this.grid.getRows().length;
		return rowLen == currentCheckLen;
	}
	,checkAll:function() {
		var that = this;
		if(this.isMultiSelect()) {
			var checked = true;
			this.$checkAll
				.prop('checked',checked)
				.trigger('doClick',[checked]);
		}
	}
	,uncheckAll:function() {
		var that = this;
		
		if(this.isMultiSelect()) {
			var checked = false;
			this.$checkAll
				.prop('checked',checked)
				.trigger('doClick',[checked]);
		}else { // 单选情况下
			var $checkboxs = this.getCheckedCheckbox();
			
			$checkboxs.each(function(){
				this.checked = false;
				$(this).trigger(that.getTriggerName());
			});
		}
	}
	,selectRow:function(index) {
		if(this.isSingleSelect()) {
			var $radio = this.getAllCheckbox().eq(index);
			var checked = $radio.prop('checked');
			if(!checked) {
				$radio.prop('checked',!checked);
				$radio.trigger(this.radioTrigger);
			}
		}
	}
	,unselectRow:function(index) {
		if(this.isSingleSelect()) {
			var $radio = this.getAllCheckbox().eq(index);
			var checked = $radio.prop('checked');
			
			if(checked) {
				$radio.prop('checked',!checked);
				$radio.trigger(this.getTriggerName());
			}
		}
	}
	,checkRow:function(index) {
		if(this.isMultiSelect()) {
			var $checkbox = this.getAllCheckbox().eq(index);
			var checked = $checkbox.prop('checked');
			if(!checked) {
				$checkbox.prop('checked',true);
				$checkbox.trigger(this.getTriggerName());
			}
		}
	}
	,uncheckRow:function(index) {
		if(this.isMultiSelect()) {
			var $checkbox = this.getAllCheckbox().eq(index);
			var checked = $checkbox.prop('checked');
			if(checked) {
				$checkbox.prop('checked',false);
				$checkbox.trigger(this.getTriggerName())
			}
		}
	}
	,selectRecord:function(idVal,doTrigger) {
		var $checkbox = this.getCheckboxByIdValue(idVal);
		var checked = $checkbox.prop('checked');
		if(!checked) {
			$checkbox.prop('checked',!checked);
			if(doTrigger) {
				$checkbox.trigger(this.getTriggerName());
			}else{
				$checkbox.trigger('active');
			}
		}
	}
	,vCellClick:function(header,row,e,index) {
		var val = row[header.field];
		var param = {
			index:index
			,head:header
			,value:val
			,row:row
			,target:e.target
		};
		this.grid.fire('ClickCell',param);
	}
	,vCellDblClick:function(header,row,e,index) {
		var val = row[header.field];
		var param = {
			index:index
			,head:header
			,value:val
			,row:row
			,target:e.target
		};
		this.grid.fire('DblClickCell',param);
	}
	,vRowClick:function(index,row,e) {
		var param = {
			index:index
			,row:row
			,target:e.target
		};
		this.grid.fire('ClickRow',param);
	}
	,vRowDblClick:function(index,row,e) {
		var param = {
			index:index
			,row:row
			,target:e.target
		};
		this.grid.fire('DblClickRow',param);
	}
	,getTriggerName:function() {
		return this.isSingleSelect() ? this.radioTrigger : this.checkboxTrigger; 
	}
	,isMultiSelect:function() {
		return !this.isSingleSelect();
	}
	,isSingleSelect:function() {
		return this.opt('singleSelect');
	}
	,getCheckType:function() {
		if(!this.checkType) {
			this.checkType = this.isSingleSelect() ? 'radio' : 'checkbox';
		}
		return this.checkType;
	}
	,getCheckedCheckbox:function() {
		return this.$wraper.find('input[name="' + this.idField + '"]:checked');
	}
	,getAllCheckbox:function() {
		return this.$wraper.find('input[name="' + this.idField + '"]');
	}
	,getUnCheckCheckbox:function() {
		return this.$wraper.find('input[name="' + this.idField + '"]:not(:checked)');
	}
	,getCheckboxByIdValue:function(idVal) {
		return this.$wraper.find('input[value="' + idVal + '"]');
	}
	,getCheckboxByIndex:function(index) {
		return this.$wraper.find('input[data-index="' + index + '"]');
	}
	// 返回选中的索引
	,getCheckedIndex:function() {
		var $inputs = this.getCheckedCheckbox();
		var indexs = [];
		$inputs.each(function(){
			indexs.push(parseInt($(this).attr('data-index')));
		});
		return indexs;
	}
	,getTemplate:function(){
		var template = 
		'<div class="pui-datatable ui-widget">' +
			// 表格主体
			'<div class="pui-datatable-scrollable-body">' +
				'<table :style="{width:grid.getTableWidth()}">' +
					'<thead>' +
						'<tr>' + 
							'<th v-show="grid.opts.rownumbers" class="ui-state-default pui-table-rownum"></th>' + 
							'<th v-'+this.v_vuitableth+'="{header:header}" class="ui-state-default" v-for="header in headers">{{ header.title }} <span class="sortspan"></span> </th>' + 
						'</tr>' + 
					'</thead>' + 
					'<tbody v-if="grid.hasData()">' + 
						'<tr v-'+this.v_vuitabletr+'="{index:index,row:row}" ' +
							' @click="grid.view.vRowClick(index,row,$event)" ' +
							' @dblclick="grid.view.vRowDblClick(index,row,$event)" ' +
							' :class="{\'ui-datatable-odd\':grid.view.isOddTr(index)}"' + 
							' class="ui-widget-content" ' +
							'v-for="(index,row) in grid.getRows()"' +
						'>' +
							'<td v-show="grid.opts.rownumbers" class="ui-state-default pui-table-rownum">{{index+1}}</td>' + 
							'<td ' +
								'@click="grid.view.vCellClick(header,row,$event,index)" ' + 
								'@dblclick="grid.view.vCellDblClick(header,row,$event,index)" ' + 
								'v-'+this.v_vuitabletd+'="{index:index,row:row,header:header}" ' +
								'v-for="header in headers"></td>' + 
						'</tr>' +
					'</tbody>' + 
					'<tbody v-else>' +
						'<tr class="ui-widget-content"><td colspan="{{grid.opts.rownumbers ? headers.length + 1 : headers.length}}">{{{grid.opts.emptyDataMsg}}}</td></tr>' +
					'</tbody>' + 
					'<tfoot v-if="grid.opts.pagination && grid.hasData()">' +
						'<tr class="ui-widget-content">' +
							'<td style="padding:0;border-width:0;" colspan="{{grid.opts.rownumbers ? headers.length + 1 : headers.length}}">' +
								// 表格分页部分
								'<div style="border-width:0;" class="pui-paginator ui-widget-header">' + 
									'<span class="pui-paginator-last pui-paginator-element">' + 
										'<select @change="page.onChange()" v-model="page.pageSize">' +
									  		'<option v-for="val in grid.opts.pageList" :value="val">' +
											    '{{ val }}' +
											'</option>' +
									  	'</select>' + 
									'</span>' +
									'<span title="首页" @click="page.first()" :class="page.canFirst() ? \'\' : \'ui-state-disabled\'" class="vui-hover pui-paginator-first pui-paginator-element ui-state-default ui-corner-all">' +  
										'<span class="ui-icon ui-icon-seek-first">p</span>' + 
									'</span>' +
									'<span title="上一页" @click="page.pre()" :class="page.canPre() ? \'\' : \'ui-state-disabled\'" class="vui-hover pui-paginator-prev pui-paginator-element ui-state-default ui-corner-all">' +  
										'<span class="ui-icon ui-icon-seek-prev">p</span>' + 
									'</span>' +
									'<span title="下一页" @click="page.next()" :class="page.canNext() ? \'\' : \'ui-state-disabled\'" class="vui-hover pui-paginator-next pui-paginator-element ui-state-default ui-corner-all">' +  
										'<span class="ui-icon ui-icon-seek-next">p</span>' + 
									'</span>' +
									'<span title="尾页" @click="page.last()" :class="page.canLast() ? \'\' : \'ui-state-disabled\'" class="vui-hover pui-paginator-last pui-paginator-element ui-state-default ui-corner-all">' +  
										'<span class="ui-icon ui-icon-seek-end">p</span>' + 
									'</span>' +
									
									'<span style="padding:0 10px;">第{{ page.pageIndex }}/{{ page.pageCount }}页，共{{ page.total }}条数据</span>' + 
									//'<button>设置</button>' + 
								'</div>' +
						
							'</td>' +
						'</tr>' +
					'</tfoot>' +
				'</table>' +
			'</div>' +
		'</div>';
		return template;
	}
},VUI.View);

})();