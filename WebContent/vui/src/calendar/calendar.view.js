/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * @ignore
 * @class
 */
VUI.Class('CalendarView',{
	/**
	 * 构造函数
	 * @ignore
	 */
	init:function(cmp) {
		this._super(cmp);
		
		this.showFoot = this.opt('showFoot');
		// 今天
		this.today = new Date();
		this.date = new Date();
		
		this.year = this.getYear();
		this.month = this.getMonth();
		
		this.months = this.buildMonths();
		this.weekTextItems = VUI.Config.DatePick.weekTextItems;
		
		this.initMonthDays();
	}
	,getYears:function() {
		if(!this.yearItems) {
			this.yearItems = [];
			var currentYear = this.getYear();
			for(var i=-10;i<=10;i++) {
				var year = currentYear + i;
				this.yearItems.push(year);
			}
		}
		return this.yearItems;
	}
	/**
	 * 获取月
	 * @ignore
	 */
	,getMonth:function() {
		return this.date.getMonth() + 1;
	}
	/**
	 * 获取日
	 * @ignore
	 */
	,getDate:function() {
		return this.date.getDate();
	}
	,buildMonths:function() {
		var items = [];
		for(var i=1;i<=12; i++) {
			items.push(i);
		}
		return items;
	}
	/**
	 * 获取年
	 * @ignore
	 */
	,getYear:function() {
		return this.date.getFullYear();
	}
	/**
	 * 设置年
	 * @ignore
	 * @param year 年份,int型
	 */
	,setYear:function(year) {
		this.date.setYear(year);
	}
	/**
	 * 设置月
	 * @ignore
	 * @param month 月份1~12,int型
	 */
	,setMonth:function(month) {
		this.date.setMonth(month - 1);
	}
	/**
	 * 设置天
	 * @ignore
	 * @param date 天,int型
	 */
	,setDate:function(date) {
		this.date.setDate(date);
	}
	,ngChangeYear:function(){
		this.setYear(this.year);
		this.initMonthDays();
	}
	,vChangeMonth:function(){
		this.setMonth(this.month);
		this.initMonthDays();
	}
	,preMonth:function() {
		if(this.month == 1) {
			this.year--;
			this.month = 12;
		}else{
			this.month--;
		}
		this.initMonthDays();
	}
	,nextMonth:function() {
		if(this.month == 12) {
			this.year++;
			this.month = 1;
		}else{
			this.month++;
		}
		this.initMonthDays();
	}
	/**
	 * 设置时间
	 * @ignore
	 * @param dateStr 字符串日期
	 */
	,setValue:function(dateStr) {
		var date;
		if(dateStr) {
			date = VUI.Date.parse(dateStr);
		}else{
			date = new Date();
		}
		this.date = date;
	}
	,isSelected:function(dayData) {
		var year = this.getYear();
		var month = this.getMonth();
		var date = this.getDate();
		return year == dayData.y && month == dayData.m && date == dayData.d;
	}
	,vClick:function(dayData) {
		this.setYear(dayData.y);
		this.setMonth(dayData.m);
		this.setDate(dayData.d);
		
		this.cmp.fire('Click',dayData);
		
		this.cmp.runOptFun('clickHandler',dayData);
		
		this.initMonthDays();
	}
	,showCalendar:function(date) {
		this.setValue(date);
		this.initMonthDays();
		this.show();
	}
	,visiable:function() {
		return this.getWraper().is(':visible');
	}
	,vClickToday:function() {
		var dayData = this.getTodayData();
		
		this.vClick(dayData);
	}
	,vClickCancel:function() {
		var dayData = this.getTodayData();
		this.cmp.runOptFun('cancelHandler',dayData);
	}
	,getTodayData:function() {
		return {
			y:this.today.getFullYear()
			,m:this.today.getMonth() + 1
			,d:this.today.getDate()
		}
	}
	,initMonthDays:function() {
		var that = this;
		var year = this.year;
		var month = this.month;
		var beforeMonth = month-1;
		var beforeYear = year;
		if(beforeMonth < 1) {
			beforeMonth = 12;
			beforeYear--;
		}
		// 当月天数
		var daysInMonth = VUI.Date.getEndDate(year,month);
		var daysInPreMonth = VUI.Date.getEndDate(year,beforeMonth);
		// 每月1号
		var firstDayOfMonth = new Date(beforeYear,beforeMonth,1);
		// 每月1号星期几
		var firstDay = firstDayOfMonth.getUTCDay();
		// 日期
		var dayIndex = 1;
		// 日期行的索引
		var rowIndex = 0;
		this.monthDays = [];
		
		var thirdRow = {weekDays:[]};
		
		// 填充空白天数
		for(var i=firstDay;i>=0;i--){
			var day = daysInPreMonth - i;
			var style = {color:'gray'};
			
			this.appendDays(thirdRow,year,beforeMonth,day,style);
		}
		// 填充第一个星期
		for (var i=firstDay + 1;i<=6;i++){
			var day = dayIndex++;
			this.appendDays(thirdRow,year,month,day);
		}
		this.monthDays.push(thirdRow);
		// 填充剩下的天数
		while (dayIndex <= daysInMonth) {
			
			var row = {weekDays:[]};
			for (var i=0;i<=6 && dayIndex <= daysInMonth;i++){
				var day = dayIndex++;
				this.appendDays(row,year,month,day);
			}
			this.monthDays.push(row);
		}
		
	}
	,appendDays:function(row,y,m,d,style) {
		
		var dayData = {y:y,m:m,d:d};
		if(dayData) {
			dayData.style = style;
		}
		row.weekDays.push(dayData);
	}
	//@override
	,getTemplate:function(){
		var template = 
		'<div style="display: block;" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ui-shadow">' +
			'<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all">' + 
				'<a class="ui-datepicker-prev ui-corner-all"><span @click="cmp.view.preMonth()" class="ui-icon ui-icon-circle-triangle-w"></span></a>'+
				'<a class="ui-datepicker-next ui-corner-all"><span @click="cmp.view.nextMonth()" class="ui-icon ui-icon-circle-triangle-e"></span></a>' + 
				'<div class="ui-datepicker-title">' +
					'<select @change="cmp.view.ngChangeYear()" v-model="cmp.view.year">' +
						'<option v-for="year in cmp.view.getYears()" :value="year">' +
						    '{{ year }}' +
						'</option>' +
					'</select>' +
					'&nbsp;<select @change="cmp.view.vChangeMonth()" v-model="cmp.view.month">' +
						'<option v-for="month in cmp.view.months" :value="month">' +
						    '{{ month }}' +
						'</option>' +
					'</select>' +
				'</div>' + 
			'</div>' + 
			'<table class="ui-datepicker-calendar">' + 
				'<thead>' +
					'<tr>' + 
						'<th v-for="txt in cmp.view.weekTextItems"><span>{{ txt }}</span></th>' + 
					'</tr>' + 
				'</thead>' + 
					'<tbody>' + 
						'<tr v-for="row in cmp.view.monthDays">' +
							'<td v-for="dayData in row.weekDays" ' +
								':class="cmp.view.isSelected(dayData) ? \'ui-datepicker-current-day\' : \'\'" ' + 
								'data-year="dayData.y" data-month="dayData.m" data-day="dayData.d">' +
								
								'<span :class="cmp.view.isSelected(dayData) ? \'ui-state-default ui-state-highlight ui-state-active \' : \'\'" ' +
									'class="vui-hover ui-state-default" ' +
									':style="dayData.style" ' +
									'style="text-align:center;cursor:pointer;" ' +
									'@click="cmp.view.vClick(dayData)">{{dayData.d}}</span>' +
								
							'</td>' + 
						'</tr>' +
					'</tbody>' + 
			'</table>' + 
			'<div v-show="cmp.view.showFoot" class="ui-datepicker-header" style="text-align: center;">' +
				'<button @click="cmp.view.vClickToday()" class="pui-button ui-widget ui-state-default ui-corner-all pui-button-text-only" value=""><span class="pui-button-text">今天</span></button>' +
				'<button @click="cmp.view.vClickCancel()" class="pui-button ui-widget ui-state-default ui-corner-all pui-button-text-only " value=""><span class="pui-button-text">清空</span></button>' +
			'</div>' + 
		'</div>';
		return template;
	}
},VUI.View);

})();