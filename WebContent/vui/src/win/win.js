/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * 窗口<br>
 * <pre>
win = new VUI.Win({
	contentId:'win'
	,title:'标题(双击最大化)'
	,width:500
	,closable:true
	,onAfterClose:function() {
		alert('关闭后触发')
	}
	,buttons:[
	 	{text:'确定',handler:function(){
	 		alert('OK')
	 	}}
	 	,{text:'取消',handler:function(){
	 		win.hide();
	 	}}
	 ]
});
 * </pre>
 * @class VUI.Win
 * @extends VUI.Component
 */
VUI.Class('Win',{
	OPTS:{
		/**
		 * @cfg {String} contentId 窗口内容的id
		 */
		contentId:null
		/**
		 * @cfg {String} url 远程请求url,如果和contentId同时存在,则优先使用url
		 */
		,url:null
		/**
		 * @cfg {String} lazyLoad 为true,url在窗口打开的时候加载
		 */
		,lazyLoad:true
		/**
		 * @cfg {String} title 窗口标题
		 */
		,title:''
		/**
		 * @cfg {Boolean} closable 能否关闭
		 */
		,closable:true
		/**
		 * @cfg {Boolean} draggable 能否拖拽
		 */
		,draggable:true
		/**
		 * @cfg {Boolean} modal 是否显示遮罩层
		 */
		,modal:true
		/**
		 * @cfg {Boolean} shadow 是否显示窗体阴影
		 */
		,shadow:true
		/**
		 * @cfg {Boolean} maximizable 能否最大化
		 */
		,maximizable:true
		/**
		 * @cfg {Boolean} minimizable 能否最小化
		 */
		,minimizable:true
		/**
		 * @cfg {Boolean} autoHide 一开始隐藏
		 */
		,autoHide:true
		,position:'fixed'
		/**
		 * @cfg {Array} buttons 底部按钮
		 */
		,buttons:[]
	}
	// ----- 事件注释 -----
	
	/**
	 * @event onClose 在用户关闭窗口时触发,返回false终止当前操作。
	 */
	 
	/**
	 * @event AfterClose 在用户关闭窗口后触发。
	 */
	
	
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
	}
	/**
	 * 设置标题
	 * @param {String} title
	 */
	,setTitle:function(title) {
		this.set('title',title);
	}
	/**
	 * @ignore
	 */
	,getViewClass:function() {
		return VUI.WinView;
	}
	/**
	 * @ignore
	 */
	,hasBtns:function() {
		return this.opts.buttons && this.opts.buttons.length > 0;
	}
	/**
	 * 移动
	 * @param {Number} left
	 * @param {Number} top
	 */
	,move:function(left,top) {
		this.view.move(left,top);
	}
	/**
	 * 隐藏
	 */
	,hide:function() {
		var ret = this.fire('Close');
		if(ret === false) {
			return;
		}
		
		this.view.hide();
		
		this.fire('AfterClose');
	}
	/**
	 * 隐藏,同hide()
	 */
	,close:function() {
		this.hide();
	}
	/**
	 * 最大化
	 */
	,max:function() {
		this.view.max();
	}
	/**
	 * 最小化
	 */
	,min:function() {
		this.view.min();
	}
	/**
	 * 正常
	 */
	,normal:function() {
		this.view.normal();
	}
	/**
	 * 隐藏按钮
	 * @param {Number} index 按钮索引
	 */
	,hideBtn:function(index) {
		this.view.hideBtn(index);
	}
	/**
	 * 显示按钮
	 * @param {Number} index 按钮索引
	 */
	,showBtn:function(index) {
		this.view.showBtn(index);
	}
},VUI.Component);

})();